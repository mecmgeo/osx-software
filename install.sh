#!/usr/bin/env bash
. software.properties

function install_apps() {
	echo "*** Installing APPS ***"
	for app in $APPS; do
		if [ $(which $app|wc -l) -lt 1 ]; then
			echo -e "\n--> Installing APP '$app'"
			brew install $app
		else
			echo -e "\n--> APP '$app' already installed"
		fi
	done
}

function install_casks() {
	echo -e "\n\n\n*** Installing CASKS ***"
	for cas in $CASKS; do
		if [ $(which $cas|wc -l) -lt 1 ]; then
			echo -e "\n--> Installing CASK '$cas'"
			brew cask install $cas
		else
			echo -e "\n--> APP '$cas' already installed"
		fi
	done
}

function install_apms() {
	echo -e "\n\n\n*** Installing APM-Module ***"
	for modapm in $APMS; do
		echo -e "\n--> Installing apm '$modapm'"
		apm install -y $modapm
	done

	if [ -f package-lock.json ];then
		rm -f package-lock.json
	fi
}

function install_npms() {
	echo -e "\n\n\n*** Installing NPM-Module ***"
	for modnpm in $NPMS; do
		echo -e "\n--> Installing npm '$modnpm'"
		apm install -y $modnpm -g
	done

	if [ -f package-lock.json ];then
		rm -f package-lock.json
	fi
}


install_apps
install_casks
install_npms
install_apms

echo -e "\n\n\n Finish installing software"